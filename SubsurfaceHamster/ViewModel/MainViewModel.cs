﻿// <copyright file="MainViewModel.cs" company="Alexander Zabluda">
// Copyright (c) Alexander Zabluda. All rights reserved.
// </copyright>

namespace SubsurfaceHamster.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Media;
    using System.Windows.Media.Media3D;
    using HelixToolkit.Wpf;
    using Microsoft.Toolkit.Diagnostics;
    using Stylet;
    using SubsurfaceHamster.Model;

    /// <summary>
    /// View model for <see cref="View.MainView"/>.
    /// </summary>
    public class MainViewModel : Screen
    {
        private readonly IReservoirModelEquidistant model;
        private readonly IVolumeCalculationStrategy volumeCalculationStrategy;
        private IVolumetricUnit outputUnit;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        [Obsolete("To be used only in designer.")]
        public MainViewModel()
            : this(new ReservoirModel(), new SimpleVolumeCalculationStrategy(), new[] { new VolumetricUnit("Cubic Meters", "m³", x => x) })
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="model"> Reservoir model. </param>
        /// <param name="volumeCalculationStrategy"> Volume calculation strategy. </param>
        /// <param name="volumetricUnits"> Supported volumetric measuring units. </param>
        public MainViewModel(
            IReservoirModelEquidistant model,
            IVolumeCalculationStrategy volumeCalculationStrategy,
            IEnumerable<IVolumetricUnit> volumetricUnits)
        {
            this.model = model ?? throw new ArgumentNullException(nameof(model));
            this.volumeCalculationStrategy = volumeCalculationStrategy ?? throw new ArgumentNullException(nameof(volumeCalculationStrategy));
            this.VolumetricUnits = volumetricUnits?.ToList() ?? throw new ArgumentNullException(nameof(volumetricUnits));
            Guard.IsNotEmpty(this.VolumetricUnits, nameof(volumetricUnits));
            this.outputUnit = this.VolumetricUnits.First();
        }

        /// <summary>
        /// Gets supported volumetric measuring units.
        /// </summary>
        public IReadOnlyCollection<IVolumetricUnit> VolumetricUnits { get; }

        /// <summary>
        /// Gets or sets volumetric measuring unit for output.
        /// </summary>
        public IVolumetricUnit OutputUnit
        {
            get => this.outputUnit;
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }

                if (this.SetAndNotify(ref this.outputUnit, value))
                {
                    this.NotifyOfPropertyChange(() => this.OilGasVolumeString);
                }
            }
        }

        /// <summary>
        /// Gets formatted string describing the grid configuration.
        /// </summary>
        public string GridSizeString => $"{this.model.GridCellSize * this.model.RowCount:N0}x{this.model.GridCellSize * this.model.ColumnCount:N0} m²" +
            $" ({this.model.RowCount}x{this.model.ColumnCount})";

        /// <summary>
        /// Gets or sets fluid contact depth.
        /// </summary>
        public double FluidContactDepth
        {
            get => this.model.FluidContactDepth;
            set
            {
                this.model.FluidContactDepth = value;
                this.NotifyOfPropertyChange();
                this.NotifyOfPropertyChange(() => this.FluidContactModel3D);
                this.NotifyOfPropertyChange(() => this.OilGasVolumeString);
            }
        }

        /// <summary>
        /// Gets or sets height of reservoir.
        /// </summary>
        public double ReservoirHeight
        {
            get => this.model.Height;
            set
            {
                this.model.Height = value;
                this.NotifyOfPropertyChange();
                this.NotifyOfPropertyChange(() => this.BaseHorizonModel3D);
                this.NotifyOfPropertyChange(() => this.OilGasVolumeString);
            }
        }

        /// <summary>
        /// Gets <see cref="Model3D"/> representing top horizon's surface (mesh).
        /// </summary>
        public Model3D TopHorizonModel3D
            => this.BuildMeshModel3D(this.model.GetTopHorizonPoint, Colors.Red, 0.5);

        /// <summary>
        /// Gets <see cref="Model3D"/> representing base horizon's surface (mesh).
        /// </summary>
        public Model3D BaseHorizonModel3D
            => this.BuildMeshModel3D(this.model.GetBaseHorizonPoint, Colors.Green, 0.5);

        /// <summary>
        /// Gets <see cref="Model3D"/> representing fluid contact (rectangle).
        /// </summary>
        public Model3D FluidContactModel3D
            => this.BuildMeshModel3D(this.model.GetFluidContactPoint, Colors.Blue, 0.5);

        /// <summary>
        /// Gets formatted string for calculated oil/gas volume measured in selected unit.
        /// </summary>
        public string OilGasVolumeString
            => $"{this.OutputUnit.ConvertFromCubicMeters(this.volumeCalculationStrategy.CalculateVolume(this.model)):N0} {this.OutputUnit.Unit}";

        private GeometryModel3D BuildMeshModel3D(Func<int, int, Point3D> pointGetter, Color color, double opacity)
        {
            var meshBuilder = new MeshBuilder(generateNormals: false, generateTexCoords: false);
            meshBuilder.AddRectangularMesh(
                this.model.YieldGridCells().Select(cell => pointGetter(cell.Row, cell.Column)).ToList(),
                this.model.ColumnCount);

            var material = MaterialHelper.CreateMaterial(color, opacity);
            return new GeometryModel3D
            {
                Geometry = meshBuilder.ToMesh(freeze: true),
                Material = material,
                BackMaterial = material,
            };
        }
    }
}
