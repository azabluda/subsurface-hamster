﻿// <copyright file="ShellViewModel.cs" company="Alexander Zabluda">
// Copyright (c) Alexander Zabluda. All rights reserved.
// </copyright>

namespace SubsurfaceHamster.ViewModel
{
    using System;
    using HandyControl.Data;
    using Microsoft.Toolkit.Diagnostics;
    using Stylet;

    /// <summary>
    /// Root view model.
    /// </summary>
    public class ShellViewModel : Conductor<Screen>
    {
        private TransitionMode transitionMode = TransitionMode.Bottom2TopWithFade;

        /// <summary>
        /// Initializes a new instance of the <see cref="ShellViewModel"/> class.
        /// </summary>
        [Obsolete("To be used only in designer.")]
        public ShellViewModel()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShellViewModel"/> class.
        /// </summary>
        /// <param name="welcomeViewModel"> View model for welcome screen. </param>
        public ShellViewModel(WelcomeViewModel welcomeViewModel)
        {
            Guard.IsNotNull(welcomeViewModel, nameof(welcomeViewModel));
            welcomeViewModel.Shell = this;
            this.ActiveItem = welcomeViewModel;
        }

        /// <summary>
        /// Gets or sets <see cref="HandyControl.Data.TransitionMode"/> value. Used to trigger UI animation.
        /// </summary>
        public TransitionMode TransitionMode
        {
            get => this.transitionMode;
            set => this.SetAndNotify(ref this.transitionMode, value);
        }
    }
}
