﻿// <copyright file="WelcomeViewModel.cs" company="Alexander Zabluda">
// Copyright (c) Alexander Zabluda. All rights reserved.
// </copyright>

namespace SubsurfaceHamster.ViewModel
{
    using System;
    using System.Windows.Threading;
    using HandyControl.Data;
    using Stylet;

    /// <summary>
    /// View model for <see cref="View.WelcomeView"/>.
    /// </summary>
    public class WelcomeViewModel : Screen
    {
        private readonly Func<MainViewModel> mainViewModelFactory;
        private int progress;

        /// <summary>
        /// Initializes a new instance of the <see cref="WelcomeViewModel"/> class.
        /// </summary>
        [Obsolete("To be used only in designer.")]
        public WelcomeViewModel()
        {
            this.progress = 45;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WelcomeViewModel"/> class.
        /// </summary>
        /// <param name="mainViewModelFactory"> Factory for <see cref="MainViewModel"/>. </param>
        public WelcomeViewModel(Func<MainViewModel> mainViewModelFactory)
        {
            this.mainViewModelFactory = mainViewModelFactory ?? throw new ArgumentNullException(nameof(mainViewModelFactory));
        }

        /// <summary>
        /// Gets welcome string.
        /// </summary>
        public static string Greeting => "Welcome, SubsurfaceHamster!";

        /// <summary>
        /// Gets or sets current progress.
        /// </summary>
        public int Progress
        {
            get => this.progress;
            set => this.SetAndNotify(ref this.progress, value);
        }

        /// <summary>
        /// Gets or sets parent <see cref="ShellViewModel"/>.
        /// </summary>
        public ShellViewModel Shell { get; set; }

        /// <summary>
        /// Starts welcome animation.
        /// </summary>
        /// <inheritdoc cref="Screen.OnInitialActivate" />
        protected override void OnInitialActivate()
        {
            base.OnInitialActivate();

            var timer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(10) };
            timer.Tick += (_, _) =>
            {
                if (++this.Progress > 100)
                {
                    timer.Stop();
                    this.Shell.TransitionMode = TransitionMode.Left2RightWithFade;
                    this.Shell.ActiveItem = this.mainViewModelFactory();
                }
            };
            timer.Start();
        }
    }
}
