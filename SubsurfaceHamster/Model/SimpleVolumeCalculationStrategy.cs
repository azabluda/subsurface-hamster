﻿// <copyright file="SimpleVolumeCalculationStrategy.cs" company="Alexander Zabluda">
// Copyright (c) Alexander Zabluda. All rights reserved.
// </copyright>

namespace SubsurfaceHamster.Model
{
    using System;
    using System.Linq;
    using Microsoft.Toolkit.Diagnostics;

    /// <summary>
    /// Simple implementation of oil/gas volume calculation.
    /// </summary>
    public class SimpleVolumeCalculationStrategy : IVolumeCalculationStrategy
    {
        /// <summary>
        /// Calculate volume between two horizons and above fluid contact (in cubic meters).
        /// </summary>
        /// <param name="model"> Reservoir model. </param>
        /// <returns> Volume in cubic meters. </returns>
        public double CalculateVolume(IReservoirModel model)
        {
            Guard.IsNotNull(model, nameof(model));
            return model.YieldGridCells().Aggregate(
                0d,
                (volume, cell) =>
                {
                    // For each cell we compute its volume using simplest 0-order approximation
                    double topZ = model.GetTopHorizonPoint(cell.Row, cell.Column).Z;
                    double baseZ = model.GetBaseHorizonPoint(cell.Row, cell.Column).Z;
                    double fluidContactZ = model.GetFluidContactPoint(cell.Row, cell.Column).Z;

                    double oilHeight = fluidContactZ > topZ ? 0 : topZ - Math.Max(baseZ, fluidContactZ);
                    double cellVolume = oilHeight * model.GridCellSize * model.GridCellSize;

                    return volume + cellVolume;
                });
        }
    }
}
