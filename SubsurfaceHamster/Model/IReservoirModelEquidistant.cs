﻿// <copyright file="IReservoirModelEquidistant.cs" company="Alexander Zabluda">
// Copyright (c) Alexander Zabluda. All rights reserved.
// </copyright>

namespace SubsurfaceHamster.Model
{
    /// <summary>
    /// Reservoir model with equidistant top/base horizons and horizontal fluid contact.
    /// </summary>
    public interface IReservoirModelEquidistant : IReservoirModel
    {
        /// <summary>
        /// Gets or sets height of reservoir (in meters).
        /// Distance between top and base horizons.
        /// </summary>
        double Height { get; set; }

        /// <summary>
        /// Gets or sets depth of fluid contact (in meters).
        /// </summary>
        double FluidContactDepth { get; set; }
    }
}
