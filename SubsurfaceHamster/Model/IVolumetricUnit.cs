﻿// <copyright file="IVolumetricUnit.cs" company="Alexander Zabluda">
// Copyright (c) Alexander Zabluda. All rights reserved.
// </copyright>

namespace SubsurfaceHamster.Model
{
    /// <summary>
    /// Descriptor of volumetric measuring unit.
    /// </summary>
    public interface IVolumetricUnit
    {
        /// <summary>
        /// Gets human readable name (e.g. "cubic feet").
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets unit sign (e.g. "ft³").
        /// </summary>
        string Unit { get; }

        /// <summary>
        /// Converts from cubic meters.
        /// </summary>
        /// <param name="cubicMeters"> Volume in cubic meters. </param>
        /// <returns> Volume in this measuring unit. </returns>
        double ConvertFromCubicMeters(double cubicMeters);
    }
}
