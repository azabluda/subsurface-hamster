﻿// <copyright file="UnitConversionHelper.cs" company="Alexander Zabluda">
// Copyright (c) Alexander Zabluda. All rights reserved.
// </copyright>

namespace SubsurfaceHamster.Model
{
    /// <summary>
    /// Measuring unit conversion helper.
    /// </summary>
    public static class UnitConversionHelper
    {
        /// <summary>
        /// Convert feet to meters.
        /// </summary>
        /// <param name="feet"> Length in feet. </param>
        /// <returns> Length in meters. </returns>
        public static double FeetToMeters(double feet) => 0.3048 * feet;

        /// <summary>
        /// Convert m³ to ft³.
        /// </summary>
        /// <param name="cubicMeters"> Volume in m³. </param>
        /// <returns> Volume in ft³. </returns>
        public static double CubicMetersToCubicFeet(double cubicMeters) => 35.3147 * cubicMeters;

        /// <summary>
        /// Convert m³ to bbl.
        /// </summary>
        /// <param name="cubicMeters"> Volume in m³. </param>
        /// <returns> Volume in bbl. </returns>
        public static double CubicMetersToBarrels(double cubicMeters) => 6.289814 * cubicMeters;
    }
}
