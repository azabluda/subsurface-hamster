﻿// <copyright file="IReservoirModel.cs" company="Alexander Zabluda">
// Copyright (c) Alexander Zabluda. All rights reserved.
// </copyright>

namespace SubsurfaceHamster.Model
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Media.Media3D;

    /// <summary>
    /// Reservoir model with two horizons and fluid contact defined an rectangular grid.
    /// </summary>
    public interface IReservoirModel
    {
        /// <summary>
        /// Gets size of grid cell (in meters).
        /// </summary>
        double GridCellSize { get; }

        /// <summary>
        /// Gets grid row count.
        /// </summary>
        int RowCount { get; }

        /// <summary>
        /// Gets grid column count.
        /// </summary>
        int ColumnCount { get; }

        /// <summary>
        /// Gets spatial coordinates of point on top horizon (in meters).
        /// </summary>
        /// <param name="row"> Grid row. </param>
        /// <param name="col"> Grid col. </param>
        /// <returns> Spatial coordinates of point on top horizon. </returns>
        Point3D GetTopHorizonPoint(int row, int col);

        /// <summary>
        /// Gets spatial coordinates of point on base horizon (in meters).
        /// </summary>
        /// <param name="row"> Grid row. </param>
        /// <param name="col"> Grid col. </param>
        /// <returns> Spatial coordinates of point on base horizon. </returns>
        Point3D GetBaseHorizonPoint(int row, int col);

        /// <summary>
        /// Gets spatial coordinates of point on fluid contact plane (in meters).
        /// </summary>
        /// <param name="row"> Grid row. </param>
        /// <param name="col"> Grid col. </param>
        /// <returns> Spatial coordinates of point on fluid contact plane. </returns>
        Point3D GetFluidContactPoint(int row, int col);

        /// <summary>
        /// Iterates and returns all grid cells.
        /// </summary>
        /// <returns> Sequence of grid cells. </returns>
        IEnumerable<(int Row, int Column)> YieldGridCells()
            => from row in Enumerable.Range(0, this.RowCount)
               from col in Enumerable.Range(0, this.ColumnCount)
               select (row, col);
    }
}
