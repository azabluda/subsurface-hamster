﻿// <copyright file="IVolumeCalculationStrategy.cs" company="Alexander Zabluda">
// Copyright (c) Alexander Zabluda. All rights reserved.
// </copyright>

namespace SubsurfaceHamster.Model
{
    /// <summary>
    /// Oil/gas volume calculation strategy.
    /// </summary>
    public interface IVolumeCalculationStrategy
    {
        /// <summary>
        /// Calculate volume between two horizons and above fluid contact (in cubic meters).
        /// </summary>
        /// <param name="model"> Reservoir model. </param>
        /// <returns> Volume in cubic meters. </returns>
        double CalculateVolume(IReservoirModel model);
    }
}
