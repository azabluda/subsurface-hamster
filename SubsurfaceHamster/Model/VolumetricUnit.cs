﻿// <copyright file="VolumetricUnit.cs" company="Alexander Zabluda">
// Copyright (c) Alexander Zabluda. All rights reserved.
// </copyright>

namespace SubsurfaceHamster.Model
{
    using System;

    /// <summary>
    /// Descriptor of volumetric measuring unit.
    /// </summary>
    public class VolumetricUnit : IVolumetricUnit
    {
        private readonly Func<double, double> convertFromCubicMeters;

        /// <summary>
        /// Initializes a new instance of the <see cref="VolumetricUnit"/> class.
        /// </summary>
        /// <param name="name"> Human readable name (e.g. "cubic feet"). </param>
        /// <param name="unit"> Unit symbol (e.g. "ft³"). </param>
        /// <param name="convertFromCubicMeters"> Conversion function from cubic meters. </param>
        public VolumetricUnit(string name, string unit, Func<double, double> convertFromCubicMeters)
        {
            this.Name = name ?? throw new ArgumentNullException(nameof(name));
            this.Unit = unit ?? throw new ArgumentNullException(nameof(unit));
            this.convertFromCubicMeters = convertFromCubicMeters
                ?? throw new ArgumentNullException(nameof(convertFromCubicMeters));
        }

        /// <summary>
        /// Gets human readable name (e.g. "cubic feet").
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Gets unit symbol (e.g. "ft³").
        /// </summary>
        public string Unit { get; }

        /// <summary>
        /// Converts from cubic meters.
        /// </summary>
        /// <param name="cubicMeters"> Volume in cubic meters. </param>
        /// <returns> Volume in this measuring unit. </returns>
        public double ConvertFromCubicMeters(double cubicMeters)
            => this.convertFromCubicMeters(cubicMeters);

        /// <inheritdoc/>
        public override string ToString() => this.Name;
    }
}
