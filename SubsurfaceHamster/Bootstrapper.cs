﻿// <copyright file="Bootstrapper.cs" company="Alexander Zabluda">
// Copyright (c) Alexander Zabluda. All rights reserved.
// </copyright>

namespace SubsurfaceHamster
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Windows;
    using Stylet;
    using StyletIoC;
    using SubsurfaceHamster.Model;
    using SubsurfaceHamster.ViewModel;

    /// <summary>
    /// Application bootstrapper.
    /// </summary>
    public class Bootstrapper : Bootstrapper<ShellViewModel>
    {
        /// <summary>
        /// Add types to IoC container.
        /// </summary>
        /// <param name="builder"> StyletIoC builder to use to configure the container. </param>
        protected override void ConfigureIoC(IStyletIoCBuilder builder)
        {
            base.ConfigureIoC(builder);

            builder.Bind<IReservoirModel>().And<IReservoirModelEquidistant>().To<ReservoirModel>();
            builder.Bind<IVolumeCalculationStrategy>().To<SimpleVolumeCalculationStrategy>();

            // Register volumetric units
            // NOTE: StyletIoC doesn't seem to allow binding multiple INSTANCES to a single service
            // https://github.com/canton7/Stylet/wiki/StyletIoC-Configuration#user-content-binding-multiple-types-to-a-single-service
            builder.Bind<IEnumerable<IVolumetricUnit>>().ToInstance(
                new[]
                {
                    new VolumetricUnit("Cubic Meters", "m³", x => x),
                    new VolumetricUnit("Cubic Feet", "ft³", UnitConversionHelper.CubicMetersToCubicFeet),
                    new VolumetricUnit("Barrels", "bbl", UnitConversionHelper.CubicMetersToBarrels),
                });
        }

        /// <summary>
        /// Ensure the current culture passed into bindings is the OS culture.
        /// By default, WPF uses en-US as the culture, regardless of the system settings.
        /// </summary>
        protected override void Launch()
        {
            FrameworkElement.LanguageProperty.OverrideMetadata(
                typeof(FrameworkElement),
                new FrameworkPropertyMetadata(
                    System.Windows.Markup.XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));

            base.Launch();
        }
    }
}
