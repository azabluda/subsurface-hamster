﻿// <copyright file="App.xaml.cs" company="Alexander Zabluda">
// Copyright (c) Alexander Zabluda. All rights reserved.
// </copyright>

namespace SubsurfaceHamster
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for App.xaml.
    /// </summary>
    public partial class App : Application
    {
    }
}
