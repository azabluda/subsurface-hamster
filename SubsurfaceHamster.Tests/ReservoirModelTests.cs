﻿// <copyright file="ReservoirModelTests.cs" company="Alexander Zabluda">
// Copyright (c) Alexander Zabluda. All rights reserved.
// </copyright>

namespace SubsurfaceHamster.Tests
{
    using System;
    using System.Windows.Media.Media3D;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using SubsurfaceHamster.Model;

    [TestClass]
    public class ReservoirModelTests
    {
        // Arrange
        private IReservoirModel Model { get; } = new ReservoirModel();

        [TestMethod]
        public void Test_Matrix_Is_Fully_Loaded()
        {
            // Assert
            Assert.AreEqual(26, this.Model.RowCount);
            Assert.AreEqual(16, this.Model.ColumnCount);
        }

        [TestMethod]
        public void GetTopHorizonPoint_Returns_Value()
        {
            // Act
            var point = this.Model.GetTopHorizonPoint(0, 0);

            // Assert
            Assert.AreEqual(new Point3D(0, 0, -3020.8728), point);
        }

        [TestMethod]
        public void GetBaseHorizonPoint_Returns_Value()
        {
            // Act
            var point = this.Model.GetBaseHorizonPoint(0, 0);

            // Assert
            Assert.AreEqual(new Point3D(0, 0, -3120.8728), point);
        }

        [TestMethod]
        public void GetFluidContactPoint_Returns_Value()
        {
            // Act
            var point = this.Model.GetFluidContactPoint(0, 0);

            // Assert
            Assert.AreEqual(new Point3D(0, 0, -3000), point);
        }

        [TestMethod]
        public void Should_Throw_On_Negative_Grid_Row()
        {
            // Act / Assert
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => this.Model.GetFluidContactPoint(-1, 0));
        }

        [TestMethod]
        public void Should_Throw_On_Invalid_Grid_Row()
        {
            // Act / Assert
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => this.Model.GetFluidContactPoint(1000, 0));
        }
    }
}
