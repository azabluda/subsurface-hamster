﻿// <copyright file="MainViewModelTests.cs" company="Alexander Zabluda">
// Copyright (c) Alexander Zabluda. All rights reserved.
// </copyright>

namespace SubsurfaceHamster.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using SubsurfaceHamster.Model;
    using SubsurfaceHamster.ViewModel;

    [TestClass]
    public class MainViewModelTests
    {
        [TestMethod]
        public void Should_Throw_On_Null_ReservoirModel()
            => Assert.ThrowsException<ArgumentNullException>(
                () => new MainViewModel(null, new SimpleVolumeCalculationStrategy(), Enumerable.Empty<IVolumetricUnit>()));

        [TestMethod]
        public void Should_Throw_On_Null_VolumeCalculationStrategy()
            => Assert.ThrowsException<ArgumentNullException>(
                () => new MainViewModel(new ReservoirModel(), null, Enumerable.Empty<IVolumetricUnit>()));

        [TestMethod]
        public void Should_Throw_On_Null_VolumetricUnits()
            => Assert.ThrowsException<ArgumentNullException>(
                () => new MainViewModel(new ReservoirModel(), new SimpleVolumeCalculationStrategy(), null));

        [TestMethod]
        public void Should_Throw_On_Empty_VolumetricUnits()
            => Assert.ThrowsException<ArgumentException>(
                () => new MainViewModel(new ReservoirModel(), new SimpleVolumeCalculationStrategy(), Enumerable.Empty<IVolumetricUnit>()));

        [TestMethod]
        public void Set_FluidContactLevel_Must_Notify_FluidContactModel3D_And_OilGasVolumeString()
        {
            // Arrange
            var viewModel = CreateTestViewModel();
            var notifiedProps = new List<string>();
            viewModel.PropertyChanged += (s, e) => notifiedProps.Add(e.PropertyName);

            // Act
            viewModel.FluidContactDepth += 1;

            // Assert
            CollectionAssert.AreEqual(
                new[]
                {
                    nameof(MainViewModel.FluidContactDepth),
                    nameof(MainViewModel.FluidContactModel3D),
                    nameof(MainViewModel.OilGasVolumeString),
                },
                notifiedProps);
        }

        [TestMethod]
        public void Set_ReservoirHeight_Must_Notify_BaseHorizonModel3D_And_OilGasVolumeString()
        {
            // Arrange
            var viewModel = CreateTestViewModel();
            var notifiedProps = new List<string>();
            viewModel.PropertyChanged += (s, e) => notifiedProps.Add(e.PropertyName);

            // Act
            viewModel.ReservoirHeight += 1;

            // Assert
            CollectionAssert.AreEqual(
                new[]
                {
                    nameof(MainViewModel.ReservoirHeight),
                    nameof(MainViewModel.BaseHorizonModel3D),
                    nameof(MainViewModel.OilGasVolumeString),
                },
                notifiedProps);
        }

        [TestMethod]
        public void Set_OutputUnit_Must_Notify_OilGasVolumeString()
        {
            // Arrange
            var viewModel = CreateTestViewModel();
            var notifiedProps = new List<string>();
            viewModel.PropertyChanged += (s, e) => notifiedProps.Add(e.PropertyName);

            // Act
            viewModel.OutputUnit = CreateVolumetricUnit();

            // Assert
            CollectionAssert.AreEqual(
                new[]
                {
                    nameof(MainViewModel.OutputUnit),
                    nameof(MainViewModel.OilGasVolumeString),
                },
                notifiedProps);
        }

        private static MainViewModel CreateTestViewModel()
            => new(
                new ReservoirModel(),
                new SimpleVolumeCalculationStrategy(),
                new[] { CreateVolumetricUnit() });

        private static IVolumetricUnit CreateVolumetricUnit()
            => new VolumetricUnit("Cubic Meters", "m³", x => x);
    }
}
