﻿// <copyright file="WelcomeViewModelTests.cs" company="Alexander Zabluda">
// Copyright (c) Alexander Zabluda. All rights reserved.
// </copyright>

namespace SubsurfaceHamster.Tests
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using SubsurfaceHamster.ViewModel;

    [TestClass]
    public class WelcomeViewModelTests
    {
        [TestMethod]
        public void Greeting_Must_Mention_Hamster()
        {
            StringAssert.Contains(WelcomeViewModel.Greeting, "hamster", StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
