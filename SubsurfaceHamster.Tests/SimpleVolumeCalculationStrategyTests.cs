﻿// <copyright file="SimpleVolumeCalculationStrategyTests.cs" company="Alexander Zabluda">
// Copyright (c) Alexander Zabluda. All rights reserved.
// </copyright>

namespace SubsurfaceHamster.Tests
{
    using System;
    using System.Windows.Media.Media3D;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using SubsurfaceHamster.Model;

    [TestClass]
    public class SimpleVolumeCalculationStrategyTests
    {
        // Arrange
        private SimpleVolumeCalculationStrategy CalculationStrategy { get; } = new();

        [TestMethod]
        public void Should_Throw_On_Null_Model()
        {
            // Act / Assert
            Assert.ThrowsException<ArgumentNullException>(() => this.CalculationStrategy.CalculateVolume(null));
        }

        [TestMethod]
        public void Should_Calculate_Correct_Volume()
        {
            // Act
            double volume = this.CalculationStrategy.CalculateVolume(new SimpleModel());

            // Assert
            Assert.AreEqual(9, volume);
        }

        private class SimpleModel : IReservoirModel
        {
            public double GridCellSize => 1;

            public int RowCount => 3;

            public int ColumnCount => 3;

            public Point3D GetTopHorizonPoint(int row, int col) => new(row, col, 2);

            public Point3D GetBaseHorizonPoint(int row, int col) => new(row, col, 0);

            public Point3D GetFluidContactPoint(int row, int col) => new(row, col, 1);
        }
    }
}
