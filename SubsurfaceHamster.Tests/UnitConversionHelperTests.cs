﻿// <copyright file="UnitConversionHelperTests.cs" company="Alexander Zabluda">
// Copyright (c) Alexander Zabluda. All rights reserved.
// </copyright>

namespace SubsurfaceHamster.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using SubsurfaceHamster.Model;

    [TestClass]
    public class UnitConversionHelperTests
    {
        [TestMethod]
        public void Convert_One_Foot_To_Meters()
        {
            // Arrange
            double feet = 1;

            // Act
            double meters = UnitConversionHelper.FeetToMeters(feet);

            // Assert
            Assert.AreEqual(0.3048, meters);
        }

        [TestMethod]
        public void Convert_One_Cubic_Meter_To_Cubic_Feet()
        {
            // Arrange
            double cubicMeter = 1;

            // Act
            double cubicFeet = UnitConversionHelper.CubicMetersToCubicFeet(cubicMeter);

            // Assert
            Assert.AreEqual(35.3147, cubicFeet);
        }

        [TestMethod]
        public void Convert_One_Cubic_Meter_To_Barrels()
        {
            // Arrange
            double cubicMeter = 1;

            // Act
            double barrels = UnitConversionHelper.CubicMetersToBarrels(cubicMeter);

            // Assert
            Assert.AreEqual(6.289814, barrels);
        }
    }
}
